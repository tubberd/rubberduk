#include<rubberduk.hpp>

#include<fstream>
#include<sstream>
#include<iostream>
#include<ctime>

using namespace rubberduk;

DukContext::DukContext(std::ostream* output_item)
{
    this->m_ContextHandle = duk_create_heap_default();
}

DukContext::DukContext(DukContext& dukcontext_item, std::ostream* output_item)
{

}

DukContext::~DukContext()
{
    duk_destroy_heap(this->m_ContextHandle);
}

void DukContext::eval(std::string code_item)
{
    duk_context* ctx = this->m_ContextHandle;
    if(duk_peval_string(ctx, code_item.c_str())!=0)
    {
        std::cout << duk_safe_to_string(ctx, -1) << std::endl;
    }
}
void DukContext::evalFile(std::string filename_item)
{
    duk_context* ctx = this->m_ContextHandle;
    time_t t = time(0);   // get time now
    tm * now = localtime(&t);
    if(duk_peval_file(ctx, filename_item.c_str())!=0)
    {
        std::cout << duk_safe_to_string(ctx, -1) << std::endl;
    }
}

void DukContext::compile(std::string outfilename_item, std::string code_item)
{
    //TODO implement
}
void DukContext::compileFile(std::string otufilename_item, std::string infilename_item)
{
    //TODO implement
}
void DukContext::load(std::string headerfilename_item)
{
    duk_context* ctx = this->m_ContextHandle;
}

void DukContext::add(DukClass& dukclass_item)
{
    duk_context* ctx = this->m_ContextHandle;
    std::unordered_map<std::string, duk_function_t> class_methods;
}
void DukContext::add(DukModule& dukmodule_item)
{

}
void DukContext::add(DukFunction& dukfunction_item)
{
    duk_context* ctx = this->m_ContextHandle;
    duk_function_t* function_item = new duk_function_t(dukfunction_item.getFunction());
    std::string name_item = dukfunction_item.getName();
    if(!m_RegisteredFunctions.count(name_item))
    {
        duk_push_c_function(ctx, &DukContext::handleFunction, DUK_VARARGS);
        duk_push_pointer(ctx, (void*)function_item);
        duk_put_prop_string(ctx, 0, "\xff""\xff""__function_item");
        duk_put_global_string(ctx, name_item.c_str());
        m_RegisteredFunctions[name_item] = function_item;
    }
    else
    {
        delete function_item;
    }
}

duk_ret_t DukContext::handleFunction(duk_context* ctx)
{
    duk_push_current_function(ctx);
    duk_get_prop_string(ctx, -1, "\xff""\xff""__function_item");
    duk_function_t* func = (duk_function_t*)duk_require_pointer(ctx, -1);
    duk_pop_2(ctx);
    return (*func)(ctx);
}
duk_ret_t DukContext::handleMethod(duk_context* ctx)
{
    duk_push_current_function(ctx);
    duk_get_prop_string(ctx, -1, "\xff""\xff""__method_item");
    duk_function_t* meth = (duk_function_t*)duk_require_pointer(ctx, -1);
    duk_pop_2(ctx);
    return (*meth)(ctx);
}
duk_ret_t DukContext::handleConstructor(duk_context* ctx)
{

}
